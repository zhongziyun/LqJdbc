package test;

import java.util.List;

import com.lq.util.jdbc.DriverClassName;
import com.lq.util.jdbc.Jdbc;
import com.lq.util.jdbc.LqDBConfig;

public class AccessODBC {
	public static void main(String[] args) {
		org.apache.log4j.PropertyConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.properties");
		com.lq.util.jdbc.PropertyConfigurator.configure("config/lqjdbc.properties");

		Jdbc.JDBC_NO_PROPERTIES=true;
		LqDBConfig config=new LqDBConfig();
		config.setDriverClassName(DriverClassName.ACCESSODBC.getName());
		config.setUrl("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=D:\\main.mdb");
		config.setUsername("admin");
		config.setPwd("@L7dGeM&");
		
		Jdbc.propsDSMap.put(Jdbc.defaultName, Jdbc.configToMap(config));
		
		List list = Jdbc.find("select * from QYXX");
		
		System.out.println(list);
		//int c = Jdbc.execute("insert into Record ( WZName,IPAddr,WDZ,SDZ,DT,KFSX, GPSJD,GPSWD,SJWDZ,SJSDZ,DevID, Tasks,DevState,DataType ) values ( '','344-01',9.1,1101.0,'2022-02-15 16:33:00','', '0.000000','0.000000',9.1,1101.0,'8978878', '4','1','1' )",new Object[] {});
		//System.out.println(c);
	}

}
