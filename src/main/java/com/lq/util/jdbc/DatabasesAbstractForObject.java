package com.lq.util.jdbc;

public abstract class DatabasesAbstractForObject {
	
	public abstract Object oracle();
	public abstract Object mysql();
	public abstract Object sqlserver();
	public abstract Object sqlLite();
	public abstract Object postgresql();
	public abstract Object access();

}
