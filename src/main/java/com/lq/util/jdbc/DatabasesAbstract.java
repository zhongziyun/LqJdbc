package com.lq.util.jdbc;

public abstract class DatabasesAbstract {

	public abstract String oracle();
	public abstract String mysql();
	public abstract String sqlserver();
	public abstract String sqlLite();
	public abstract String postgresql();
	public abstract String access();
}
