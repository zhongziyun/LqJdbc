package com.lq.util.jdbc;

import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * 数据库链接池<br>
 * 默认读取lqjdbc.properties文件<br>
 * @author 吃人的肉
 * QQ:376870344<br>
 * email:liuqingrou@163.com
 */
public class LqDBOperator {
	
	private LqDBOperator(){}
	
	protected static ComboPooledDataSource config(Map<String,String> map) {
		ComboPooledDataSource dsWebgameDs = new ComboPooledDataSource();
		try {
			dsWebgameDs.setDriverClass(map.get("driverClassName"));
		} catch (PropertyVetoException e) {
			e.printStackTrace();
		}
		// 参数由 Config 类根据配置文件读取
		dsWebgameDs.setJdbcUrl(map.get("url"));// 设置JDBC的URL
		dsWebgameDs.setUser(map.get("username"));// 设置数据库的登录用户名
		dsWebgameDs.setPassword(map.get("pwd"));// 设置数据库的登录用户密码
		Integer MaxPoolSize=1;
		if(map.get("MaxPoolSize")!=null && !map.get("MaxPoolSize").trim().equals("")) {
			MaxPoolSize=Integer.valueOf(map.get("MaxPoolSize"));
		}
		dsWebgameDs.setMaxPoolSize(MaxPoolSize);// 设置连接池的最大连接数
		Integer MinPoolSize=1;
		if(map.get("MinPoolSize")!=null && !map.get("MinPoolSize").trim().equals("")) {
			MinPoolSize=Integer.valueOf(map.get("MinPoolSize"));
		}
		dsWebgameDs.setMinPoolSize(MinPoolSize);// 设置连接池的最小连接数
		Integer InitialPoolSize=1;
		if(map.get("InitialPoolSize")!=null && !map.get("InitialPoolSize").trim().equals("")) {
			InitialPoolSize=Integer.valueOf(map.get("InitialPoolSize"));
		}
		dsWebgameDs.setInitialPoolSize(InitialPoolSize);//设置连接池的初始化连接数
		Integer NumHelperThreads=1;
		if(map.get("NumHelperThreads")!=null && !map.get("NumHelperThreads").trim().equals("")) {
			NumHelperThreads=Integer.valueOf(map.get("NumHelperThreads"));
		}
		dsWebgameDs.setNumHelperThreads(NumHelperThreads);//设置线程数量，默认是3
		dsWebgameDs.setAutoCommitOnClose(false);//连接关闭时默认将所有未提交的操作回滚
		dsWebgameDs.setTestConnectionOnCheckin(true);// 检查连接的有效性
		dsWebgameDs.setTestConnectionOnCheckout(false);//如果为true，在连接释放的同事将校验连接的有效性。
		dsWebgameDs.setAcquireRetryAttempts(0);//获取连接失败后再尝试10次，小于等于0则无限重试直至连接获得成功，再失败则返回DAOException异常
		dsWebgameDs.setAcquireRetryDelay(1000);//连接池在获得新连接时的间隔时间。default : 1000 单位ms
		dsWebgameDs.setMaxStatements(0);// 有时候会产生死锁，将maxStatements设置为0
		dsWebgameDs.setAcquireIncrement(3);//当连接池中的连接耗尽的时候c3p0一次同时获取的连接数
		dsWebgameDs.setIdleConnectionTestPeriod(60);//每60秒检查所有连接池中的空闲连接
		dsWebgameDs.setMaxIdleTime(60);//最大空闲时间,60秒内未使用则连接被丢弃
		int CheckoutTimeout=10000;
		if(map.get("CheckoutTimeout")!=null && !map.get("CheckoutTimeout").trim().equals("")) {
			CheckoutTimeout=Integer.valueOf(map.get("CheckoutTimeout"));
		}
		dsWebgameDs.setCheckoutTimeout(CheckoutTimeout);//当连接池用完时客户端调用getConnection()后等待获取新连接的时间，超时后将抛出SQLException,如设为0则无限期等待。单位毫秒。
		return dsWebgameDs;
	}
	
}
