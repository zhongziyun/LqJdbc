package com.lq.util.jdbc;


/**
 * 数据配置信息
 * @author 吃人的肉
 * QQ:376870344<br>
 * email:liuqingrou@163.com
 */
public class LqDBConfig {
	
	private String driverClassName="";
	private String url="";
	private String username="";
	private String pwd="";
	private String pageSize="";
	private String groupPageSize="";
	private String sqlLog="";
	private String sqlSuccessTime="";
	private String maxPoolSize=null;
	private String minPoolSize=null;
	private String initialPoolSize=null;
	private String numHelperThreads=null;
	private String checkoutTimeout=null;
	
	
	
	public String getMaxPoolSize() {
		return maxPoolSize;
	}
	public void setMaxPoolSize(String maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}
	public String getMinPoolSize() {
		return minPoolSize;
	}
	public void setMinPoolSize(String minPoolSize) {
		this.minPoolSize = minPoolSize;
	}
	public String getInitialPoolSize() {
		return initialPoolSize;
	}
	public void setInitialPoolSize(String initialPoolSize) {
		this.initialPoolSize = initialPoolSize;
	}
	public String getNumHelperThreads() {
		return numHelperThreads;
	}
	public void setNumHelperThreads(String numHelperThreads) {
		this.numHelperThreads = numHelperThreads;
	}
	public String getCheckoutTimeout() {
		return checkoutTimeout;
	}
	public void setCheckoutTimeout(String checkoutTimeout) {
		this.checkoutTimeout = checkoutTimeout;
	}
	public String getDriverClassName() {
		return driverClassName;
	}
	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getPageSize() {
		return pageSize;
	}
	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}
	public String getGroupPageSize() {
		return groupPageSize;
	}
	public void setGroupPageSize(String groupPageSize) {
		this.groupPageSize = groupPageSize;
	}
	public String getSqlLog() {
		return sqlLog;
	}
	public void setSqlLog(String sqlLog) {
		this.sqlLog = sqlLog;
	}
	public String getSqlSuccessTime() {
		return sqlSuccessTime;
	}
	public void setSqlSuccessTime(String sqlSuccessTime) {
		this.sqlSuccessTime = sqlSuccessTime;
	}
	
	

}
