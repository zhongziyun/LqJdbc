package com.lq.util.jdbc;

public enum DriverClassName {
	
	MYSQL7("com.mysql.jdbc.Driver"),
	MYSQL8("com.mysql.cj.jdbc.Driver"),
	ORACLE("oracle.jdbc.driver.OracleDriver"),
	SQLITE("org.sqlite.JDBC"),
	SQLSERVER("com.microsoft.jdbc.sqlserver.SQLServerDriver"),
	POSTGRESQL("org.postgresql.Driver"),
	ACCESSODBC("sun.jdbc.odbc.JdbcOdbcDriver"),
	ACCESS("net.ucanaccess.jdbc.UcanaccessDriver")
	;
	
	
	private String name;
	
	private DriverClassName(String name) {
		this.name=name;
	}
	
	public String getName() {
		return name;
	}

}
