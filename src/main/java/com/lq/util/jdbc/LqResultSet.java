package com.lq.util.jdbc;


/**
 * 结果集接口
 * @author 吃人的肉
 * QQ:376870344<br>
 * email:liuqingrou@163.com
 */
public interface LqResultSet {
	public void getResultSet(LqJdbcFactory jdbc);
}
