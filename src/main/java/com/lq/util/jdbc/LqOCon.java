package com.lq.util.jdbc;

import java.sql.Connection;

/**
 * 寄生模式接口
 * @author 吃人的肉
 * QQ:376870344<br>
 * email:liuqingrou@163.com
 */
public interface LqOCon {
	
	public Connection getCon();

}
