package test;

import java.util.HashMap;
import java.util.Map;


import com.lq.util.jdbc.Jdbc;

/**
 * 无配制文件化
 * @author 吃人的肉
 * QQ:376870344<br>
 * email:liuqingrou@163.com
 */
//@Configuration
public class JdbcNoProsConfig {
	
	private String username;
	
	
	//@Value("${server.port}")
	private void setUsername(String username) {
		this.username=username;
	}
	
	//@Value("${email.smtpport}")
	private String pwd;
	
	//@Value("${email.smtpport}")
	private String url;
	
	//@Value("${email.smtpport}")
	private String driverClassName;
	
	//@Bean
	public void jdbcNoProsConfigInit(){
		Jdbc.JDBC_NO_PROPERTIES=false;
		Map propsMap=new HashMap();
		propsMap.put("driverClassName", driverClassName);
		propsMap.put("pageSize", "15");
		propsMap.put("groupPageSize", "5");
		propsMap.put("sqlLog", "true");
		propsMap.put("sqlSuccessTime", "true");
		propsMap.put("url", url);
		propsMap.put("username", username);
		propsMap.put("pwd", pwd);
		propsMap.put("MaxPoolSize", "10");
		propsMap.put("MinPoolSize", "5");
		propsMap.put("InitialPoolSize", "5");
		propsMap.put("NumHelperThreads", "3");
		Jdbc.propsDSMap.put(Jdbc.defaultName, propsMap);
	}

}
