# LqJdbc

#### 介绍
数据库操作的轻量级开源项目

#### 软件架构
软件架构说明

#### 特色功能
可以开启寄生模式，跟任何项目完全融合。（详细见使用说明）
可以无配制文件（详细见使用说明）

#### 安装教程

1.  将源代码复制到项目中
2.  [将配制文件复制到项目中（lqjdbc.properties）]
3.  配置数据源（修改lqjdbc.properties）

#### 使用说明
```
--配置文件

#mysql7 com.mysql.jdbc.Driver
#mysql8 com.mysql.cj.jdbc.Driver
DriverClassName=com.mysql.jdbc.Driver
AllUrl=jdbc:mysql://127.0.0.1:3306/ley?useSSL=false&serverTimezone=UTC&useOldAliasMetadataBehavior=true&useUnicode=true&characterEncoding=utf-8&rewriteBatchedStatements=true&allowMultiQueries=true***root***root
#初始化链接数
InitialPoolSize=0
#最小链接数
MinPoolSize=0
#最大链接数
MaxPoolSize=1
#执行线程数-版本支持2.3以上版本
NumHelperThreads=1
#每页显示多少条数据
PageSize=15
#多少页一组
GroupPageSize=5
#打印日志
SqlLog=true
#是否打印查询耗时-版本支持2.1.9以上版本
SqlSuccessTime=true


#^^^^^^^^^^^^^^^^^^^^^^^日志库^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
lqjdbc.appender.log.DriverClassName=org.sqlite.JDBC
lqjdbc.appender.log.AllUrl=jdbc:sqlite:log.db***root***0eDRV2TNI5hb
lqjdbc.appender.log.InitialPoolSize=0
lqjdbc.appender.log.MinPoolSize=0
lqjdbc.appender.log.MaxPoolSize=1
lqjdbc.appender.log.NumHelperThreads=1
lqjdbc.appender.log.PageSize=15
lqjdbc.appender.log.GroupPageSize=5
lqjdbc.appender.log.SqlLog=true
lqjdbc.appender.log.SqlSuccessTime=true


#^^^^^^^^^^^^^^^^^^^^^^^电影库^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
lqjdbc.appender.zzy.DriverClassName=org.sqlite.JDBC
lqjdbc.appender.zzy.AllUrl=jdbc:sqlite:zzy.db***root***0eDRV2TNI5hb
lqjdbc.appender.zzy.InitialPoolSize=50
lqjdbc.appender.zzy.MinPoolSize=20
lqjdbc.appender.zzy.MaxPoolSize=100
lqjdbc.appender.zzy.NumHelperThreads=20
lqjdbc.appender.zzy.PageSize=15
lqjdbc.appender.zzy.GroupPageSize=5
lqjdbc.appender.zzy.SqlLog=true
lqjdbc.appender.zzy.SqlSuccessTime=true


#^^^^^^^^^^^^^^^^^^^^^^^保卫地球^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#MYSQL_URL
lqjdbc.appender.xyx.AllUrl=jdbc:mysql://192.168.1.254/hhh?useOldAliasMetadataBehavior=true&useUnicode=true&characterEncoding=utf-8&rewriteBatchedStatements=true***root***376870344
#MYSQL_DRIVER
lqjdbc.appender.xyx.DriverClassName=com.mysql.jdbc.Driver
#初始化链接数
lqjdbc.appender.xyx.InitialPoolSize=5
#最小链接数
lqjdbc.appender.xyx.MinPoolSize=1
#最大链接数
lqjdbc.appender.xyx.MaxPoolSize=10
#执行线程数-版本支持2.3以上版本
lqjdbc.appender.xyx.NumHelperThreads=3
#每页显示多少条数据
lqjdbc.appender.xyx.PageSize=15
#多少页一组
lqjdbc.appender.xyx.GroupPageSize=5
#打印日志
lqjdbc.appender.xyx.SqlLog=true
#是否打印查询耗时-版本支持2.1.9以上版本
lqjdbc.appender.xyx.SqlSuccessTime=true
```


```


package test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.map.ListOrderedMap;

import com.lq.util.jdbc.DatabasesInterface;
import com.lq.util.jdbc.Jdbc;
import com.lq.util.jdbc.LqEntitySql;
import com.lq.util.jdbc.LqJdbcFactory;
import com.lq.util.jdbc.LqPiLiang;
import com.lq.util.jdbc.LqResultSet;
import com.lq.util.jdbc.LqShiWu;
import com.lq.util.jdbc.Page;



public class Test extends BaseDB{
	
	public static void main(String[] args) {
		org.apache.log4j.PropertyConfigurator.configure(System.getProperty("user.dir")+"/config/log4j.properties");
		com.lq.util.jdbc.PropertyConfigurator.configure("config/lqjdbc.properties");
	}
	
	public void pageSelectTest(){
		//SQL SERVER 2005 分页查询用法
		String sql="SELECT ROW_NUMBER() OVER (ORDER BY id) AS RowNumber,* from payinfo";
		String sqlCount="select count(*) from payinfo";
		//其它数据库SQL分页
		sql="SELECT * from payinfo";
		sqlCount="select count(*) from payinfo";
		
		Page page=Jdbc.findPage(sql, 1, 3, sqlCount, new Object[]{});
		System.out.println(page.getData().size());
	}
	
	public void shuoM(){
		/**
		 * 生成实体类
		 */
		Jdbc.createEntity("com.entity","t_user","id");
		/**
		 * 生成插入修改SQL语句
		 */
		Jdbc.createSQL("t_user","id");
		
		//-------------------------下面为SQL语句操作----------------------------------------
		
		/**
		 * 查询
		 */
		Jdbc.find("select * from t_user where id=?", new Object[]{"1"});
		/**
		 * 分页查询
		 */
		Page page=Jdbc.findPage("select * from t_user where id=?", 1, 15, "select count(id) from t_user where id=?", new Object[]{"1"});
		/**
		 * 插入
		 */
		Jdbc.execute("insert into test (name)values(?)", new Object[]{"吃人的肉"});
		/**
		 * 修改
		 */
		Jdbc.execute("update test set name=? where id=?", new Object[]{"吃人的肉1","1"});
		/**
		 * 删除
		 */
		Jdbc.execute("delete from test where id=?",new Object[]{"1"});
		
		//------------------------下面为实体类操作------------------------------------------
		
		final Test test=new Test();
		Jdbc.save(test);//插入
		Jdbc.update(test);//修改
		Jdbc.delete(test);//删除
		List<Test> list=Jdbc.find("select * from t_user", Test.class, new Object[]{});//查询返回实体类
		
		//------------------------下面为事务操作------------------------------------------
		
		/**
		 * 事务处理
		 */
		Jdbc.shiwu(new LqShiWu() {
			@Override
			public void shiwu(LqJdbcFactory jdbc) {
				if (1!=1) {//验证判断
					jdbc.shiB=9;//如果验证通过返回自定义的状态码
					return;
				}
				jdbc.find("select * from t_user where id=?", new Object[]{"1"});
				jdbc.execute("insert into test (name)values(?)", new Object[]{"吃人的肉"});
				jdbc.execute("update test set name=? where id=?", new Object[]{"吃人的肉1","1"});
				jdbc.execute("delete from test where id=?",new Object[]{"1"});
				Jdbc.save(test);//插入
				Jdbc.update(test);//修改
				Jdbc.delete(test);//删除
			}
		});
		
		/**
		 * 图片的存储<文件>
		 */
		File img = new File("pic.jpeg");
		int c = Jdbc.execute("update image set pic=?",new Object[]{img});
		img.delete();
		
		/**
		 * 图片的存储<流>
		 */
		FileInputStream in = null;
		try {
			in =new FileInputStream(new File("pic.jpeg"));
			int c2 = Jdbc.execute("update image set pic=?",new Object[]{in});
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/**
		 * 图片的读取
		 */
		List<ListOrderedMap> listImg = Jdbc.getDS("pg").find("select area_img from company_area limit 1 ");
		InputStream inImg = (InputStream) listImg.get(0).get("area_img");
		
		/**
		 * 操作结果集
		 */
		Jdbc.operationResultSet(new LqResultSet() {
			@Override
			public void getResultSet(LqJdbcFactory jdbc) {
				ResultSet rs = jdbc.findResultSet("select photo from t_user where id=?",new Object[]{"5"});
				try {
					while (rs.next()) {
						InputStream in=rs.getBinaryStream("photo");
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		
		/**
		 * 多种数据库操作
		 */
		final StringBuffer sb=new StringBuffer();
		final List listCs2=new ArrayList();
		Jdbc.sql(new DatabasesInterface() {
			public String oracle() {
				sb.append("select * from admin where id=?");
				listCs2.add("1");
				return null;
			}
			public String mysql() {
				sb.append("select * from admin where id=?");
				listCs2.add("1");
				return null;
			}
			@Override
			public String postgresql() {
				sb.append("select * from admin where id=?");
				listCs2.add("1");
				return null;
			}
			public String sqlserver() {
				sb.append("select * from admin where id=?");
				listCs2.add("1");
				return null;
			}
			public String sqlLite() {
				sb.append("select * from admin where id=?");
				listCs2.add("1");
				return null;
			}
		});
		List list2=Jdbc.find(sb.toString(),listCs2.toArray());
		System.out.println(list2);
		
		
		/**
		 * 批量执行
		 */
		final String sql="insert into test (name)values(?)";
		Jdbc.piliang(sql,new LqPiLiang() {
			public void piliang(LqJdbcFactory jdbc) {
				for (int i = 0; i < 100; i++) {
					jdbc.executeS(new Object[]{i+""});
				}
			}
		});
		
		/**
		 * 多数据源操作
		 */
		List listDdb=Jdbc.find("select * from test");//读取默认数据源
		List listDrdb=Jdbc.getDS(readDB).find("select * from test");//读取第二个数据源
		List listDwdb=Jdbc.getDS(writeDB).find("select * from test");//读取第三个数据源



		Jdbc.PARASITIC_MODE=true;//开启寄生模式
		Jdbc.oCon=new LqOCon() {
			@Override
			public Connection getCon() {
				//这里从其它ORM数据源中获取Connection
				Connection con=Jdbc.getCon();
				// TODO Auto-generated method stub
				return con;
			}
		};


		//无配置文件的用法<Map写法>
		Jdbc.JDBC_NO_PROPERTIES=true;
		Map propsMap=new HashMap();
		propsMap.put("DriverClassName", "com.mysql.jdbc.Driver");
		propsMap.put("pageSize", "15");
		propsMap.put("groupPageSize", "5");
		propsMap.put("SqlLog", "true");
		propsMap.put("SqlSuccessTime", "true");
		propsMap.put("url", "jdbc:mysql://127.0.0.1:3306/ley?		useSSL=false&serverTimezone=UTC&useOldAliasMetadataBehavior=true&useUnicode=true&characterEncoding=utf-8&rewriteBatchedStatements=true&allowMultiQueries=true");
		propsMap.put("username", "root");
		propsMap.put("pwd", "666666");
		propsMap.put("MaxPoolSize", "1");
		propsMap.put("MinPoolSize", "0");
		propsMap.put("InitialPoolSize", "0");
		propsMap.put("NumHelperThreads", "1");
		Jdbc.propsDSMap.put(Jdbc.defaultName,propsMap);
		Jdbc.propsDSMap.put("test", propsMap);//第二个数据源
		List list = Jdbc.find("select * from test");
		List list2 = Jdbc.getDS("test").find("select * from test");
		Jdbc.getDS("test").find("select * from test");
		System.out.println(list);
		System.out.println(list2);


		//无配置文件的用法<对象写法>
		Jdbc.JDBC_NO_PROPERTIES=true;
		LqDBConfig config=new LqDBConfig();
		config.setDriverClassName(DriverClassName.MYSQL7.getName());
		config.setUrl("jdbc:mysql://127.0.0.1:3306/ley?useSSL=false&serverTimezone=UTC&useOldAliasMetadataBehavior=true&useUnicode=true&characterEncoding=utf-8&rewriteBatchedStatements=true&allowMultiQueries=true");
		config.setUsername("root");
		config.setPwd("666666");
		Jdbc.propsDSMap.put("test2", Jdbc.configToMap(config));
		//实例
		List list3=Jdbc.getDS("test2").find("select * from test");
		System.out.println(list3);//输出结果集


 
		
	}
}

```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

```






```
